/**
 * Game initialization script
 *
 *  Copyright (C) 2012  Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

( function()
{
/* jshint browser:true */
/* global lasertank */

const ltg_input = document.getElementById( 'ltg' ),
      lvl_input = document.getElementById( 'lvl' ),

      load_ltg = lasertank.FileLoader( ltg_input, new window.FileReader() ),
      load_lvl = lasertank.FileLoader( lvl_input, new window.FileReader() ),

      ele_game = document.getElementById( 'game' ),
      ctx      = document.getElementById( 'render' ).getContext( '2d' );

let ltg_data = '',
    lvl_data = '';


// XXX: relies on side-effects of ctor
lasertank.ui.MenuBar( document.getElementById( 'menubar' ) );


load_ltg.onLoad( function( e, data )
{
    if ( e ) throw e;

    ltg_data = data;
    gamechk();
} );


load_lvl.onLoad( function( e, data )
{
    if ( e ) throw e;

    lvl_data = data;
    gamechk();
} );


function gamechk()
{
    if ( !( ltg_data && lvl_data ) ) return;

    // temporary
    if ( ele_game.className.search( 'opening' ) > -1 ) return;

    lasertank.ClassicGame( document, ltg_data, lvl_data )
        .on( 'ready', function()
        {
            this.renderTo( ctx, window );
        } );
}


// temporary
document.getElementById( 'new' ).onclick = function()
{
    ele_game.className = ele_game.className.replace( ' opening', '' );
    gamechk();
};

} )();
