/**
 * Contains tile definition for classic (original game) tile sets
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class   = require( 'easejs' ).Class,
      TileDfn = require( './TileDfn' );


/**
 * Definition for classic tile sets found in the original game
 */
module.exports = Class( 'ClassicTileDfn' )
    .implement( TileDfn )
    .extend(
{
    /**
     * Retrieve the tile definition
     *
     * The definition should be an array of arrays, with the first index
     * representing the tile id and the second index representing whether or not
     * a mask should be applied (0 or 1). Multiple tiles with the same id will
     * be combined into frames for animation.
     *
     * The tiles are expected to be parsed per row, beginning at the upper-left
     * corner. Specifying the tiles in this manner is both concise and helps to
     * eliminate human error and maintenance concerns that may arise from the
     * manual specification of tile coordinates.
     *
     * @return  {Array.<Array.<string,number>>}  tile definition
     */
    'public getTileDefinition': function()
    {
        return [
            [ 'dirt',   0 ], /* dirt */
            [ 'tup',    1 ], /* tank up */
            [ 'tright', 1 ], /* tank right */
            [ 'tdown',  1 ], /* tank down */
            [ 'tleft',  1 ], /* tank left */
            [ 'base',   0 ], /* base */
            [ 'base',   0 ], /* base alt */
            [ 'base',   0 ], /* base alt 2 */
            [ 'water',  0 ], /* water */
            [ 'water',  0 ], /* water alt */

            [ 'water',    0 ], /* water alt 2 */
            [ 'atdownb',  1 ], /* anti-tank, blown up, down */
            [ 'block',    0 ], /* non-movable block */
            [ 'mblock',   1 ], /* movable block */
            [ 'brick',    0 ], /* brick */
            [ 'atup',     1 ], /* anti-tank up */
            [ 'atup',     1 ], /* anti-tank up alt */
            [ 'atup',     1 ], /* anti-tank up alt 2 */
            [ 'mblockw',  0 ], /* movable block in water */
            [ 'mirrorul', 1 ], /* mirror up-left */

            [ 'mirrorur', 1 ], /* mirror up-right */
            [ 'mirrordr', 1 ], /* mirror down-right */
            [ 'mirrordl', 1 ], /* mirror down-left */
            [ 'owup',     0 ], /* one-way up */
            [ 'owup',     0 ], /* one-way up alt */
            [ 'owup',     0 ], /* one-way up alt 2 */
            [ 'owright',  0 ], /* one-way right */
            [ 'owright',  0 ], /* one-way right alt */
            [ 'owright',  0 ], /* one-way right alt 2 */
            [ 'owdown',   0 ], /* one-way down */

            [ 'owdown',  0 ], /* one-way down alt */
            [ 'owdown',  0 ], /* one-way down alt 2 */
            [ 'owleft',  0 ], /* one-way left */
            [ 'owleft',  0 ], /* one-way left alt */
            [ 'owleft',  0 ], /* one-way left alt 3 */
            [ 'atright', 1 ], /* anti-tank right */
            [ 'atright', 1 ], /* anti-tank right alt */
            [ 'atright', 1 ], /* anti-tank right alt 2 */
            [ 'atdown',  1 ], /* anti-tank down */
            [ 'atdown',  1 ], /* anti-tank down alt */

            [ 'atdown',    1 ], /* anti-tank down alt 2 */
            [ 'atleft',    1 ], /* anti-tank left */
            [ 'atleft',    1 ], /* anti-tank left alt */
            [ 'atleft',    1 ], /* anti-tank left alt 2 */
            [ 'cblock',    0 ], /* crystal block */
            [ 'cblockht',  0 ], /* crystal block hit by tank */
            [ 'rmirrorul', 0 ], /* roto-mirror up-left */
            [ 'rmirrorur', 0 ], /* roto-mirror up-right */
            [ 'rmirrordr', 0 ], /* roto-mirror down-right */
            [ 'rmirrordl', 0 ], /* roto-mirror down-left */

            [ 'cblockhat', 0 ], /* crystal block hit by anti-tank */
            [ 'atrightb',  1 ], /* anti-tank, blown up, right */
            [ 'atleftb',   1 ], /* anti-tank, blown up, left */
            [ 'atupb',     1 ], /* anti-tank, blown up, up */
            [ 'tunnel',    1 ], /* wormhole/tunnel */
            [ 'ice',       0 ], /* ice */
            [ 'thinice',   0 ]  /* thin ice */
        ];
    },


    /**
     * Retrieve tile dimensions
     *
     * This method should return an array containing three values: the width and
     * height of the individual tiles and the number of tiles per row. From this
     * data, any individual tile can be extracted from the tile set.
     *
     * @return  {Array.<number>}  tile width, tile height, tiles per row
     */
    'public getTileDimensions': function()
    {
        return [ 32, 32, 10 ];
    }
} );
