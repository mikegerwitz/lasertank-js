/**
 * Interface representing a facade for a game type
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Interface = require( 'easejs' ).Interface;


/**
 * Represents a game type
 *
 * The facade should perform all necessary loading for support of a specific
 * type of game play. All DOM elements should be injected.
 */
module.exports = Interface( 'Game',
{
    /**
     * Render to the given 2d canvas context
     *
     * @param  {CanvasRenderingContext2d}  ctx  2d canvas context
     *
     * @return  {Game}  self
     */
    'public renderTo': [ 'ctx' ],


    /**
     * Set LTG data for tiles
     *
     * @param  {string}      data      binary string containing LTG data
     * @param  {function()}  callback  function to call when complete
     *
     * @return  {Game}  self
     */
    'public setTileData': [ 'data', 'callback' ],


    /**
     * Set LVL data for levels
     *
     * @param  {string}      data      binary string containing LVL data
     * @param  {function()}  callback  function to call when complete
     *
     * @return  {Game}  self
     */
    'public setLevelData': [ 'data', 'callback' ],


    /**
     * Attach event handler
     *
     * This is a very basic system and allows for only a single event to be
     * attached at a time (that is all that is needed at this point)
     *
     * @param  {string}    name      event name
     * @param  {Function}  callback  event callback
     *
     * @return  {Game}  self
     */
    'public on': [ 'name', 'callback' ]
} );
