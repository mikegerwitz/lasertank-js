/**
 * Represents a tank game object
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class      = require( 'easejs' ).Class,
      GameObject = require( './GameObject' );


module.exports = Class( 'Tank' )
    .extend( GameObject,
{
    'override public move': function( direction, c, sc )
    {
        const state = [ 'tleft', 'tup', 'tright', 'tdown' ][ direction ];

        if ( state !== this.getTid() )
        {
            sc( state );
            return this;
        }

        // let parent handle the movement
        this.__super( direction, c, sc );

        // if we are not yet facing in the requested direction, simply turn (do
        // not move)
        return this;
    }
} );
