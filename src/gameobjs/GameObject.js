/**
 * Represents a game object
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class = require( 'easejs' ).Class;


module.exports = Class( 'GameObject',
{
    'private _tid': '',


    __construct: function( tid )
    {
        this._tid = ''+tid;
        this._pos = 0;
    },


    'public getTid': function()
    {
        return this._tid;
    },


    'public cloneTo': function( dest )
    {
        return dest;
    },


    /* jshint -W098 */
    'virtual public move': function( dir, c, sc )
    {
        // move in the appropriate direction (action has been pre-configured
        // with the correct direction)
        c();

        return this;
    }
    /* jshint +W098 */
} );
