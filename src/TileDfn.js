/**
 * Contains tile definition interface
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This interface provides a definition for the processing of tile sets.
 * Specifically, it includes the width and height of each individual tile, the
 * number of tiles per row, the ids to map each tile to and a flag indicating
 * whether or not the tile should have a mask applied to it.
 *
 * The true benefit of this interface is that it abstracts the tile
 * interpretation logic in such a way that *any* tile set is supported. That is,
 * should someone decide to support entirely separate tile sets (perhaps to
 * add additional features to the game, or even to simply restructure the tile
 * positions of the current one), we have them covered.
 *
 * Many tiles are used for the purpose of creating animations. In order to
 * support an arbitrary number of frame tiles for any type of tile, animations
 * will be created from tiles that share the same id. When a tile is encountered
 * with a duplicate id, it is added to any previous tiles to create animation
 * frames in the order in which they appear in the definition.
 *
 * Below is a list of standard ids that must be mapped for classic game play.
 *
 *    dirt      - dirt
 *    tup       - tank up
 *    tright    - tank right
 *    tdown     - tank down
 *    tleft     - tank left
 *    base      - base
 *    water     - water
 *    atdownb   - anti-tank down
 *    block     - block
 *    mblock    - movable block
 *    brick     - brick
 *    atup      - anti-tank up
 *    mblockw   - movable block in water
 *    mirrorul  - mirror up-left
 *    mirrorur  - mirror up-right
 *    mirrordr  - mirror down-right
 *    mirrordl  - mirror down-left
 *    owup      - one-way up
 *    owright   - one-way right
 *    owdown    - one-way down
 *    owleft    - one-way left
 *    atright   - anti-tank right
 *    atdown    - anti-tank down
 *    atleft    - anti-tank left
 *    cblock    - crystal block
 *    cblockht  - crystal block hit by tank
 *    rmirrorul - roto-mirror up-left
 *    rmirrorur - roto-mirror up-right
 *    rmirrordr - roto-mirror down-right
 *    rmirrordl - roto-mirror down-left
 *    cblockhat - crystal block hit by anti-tank
 *    atrightb  - anti-tank, blown up, right
 *    atleftb   - anti-tank, blown up, left
 *    atupb     - anti-tank, blown up, up
 *    tunnel    - wormhole/tunnel
 *    ice       - ice
 *    thinice   - thin ice
 */

 const Interface = require( 'easejs' ).Interface;


/**
 * Defines the contents of a tile set
 *
 * The tile set is expected to be an image of any width (W) and height (H)
 * containing individual tiles (of width w and height h) such that W % w === 0
 * and H % h === 0. All tiles are expected to share the same dimensions.
 */
module.exports = Interface( 'TileDfn',
{
    /**
     * Retrieve the tile definition
     *
     * The definition should be an array of arrays, with the first index
     * representing the tile id and the second index representing whether or not
     * a mask should be applied (0 or 1). A list of standard tile ids are
     * provided at the top of this file. Multiple tiles with the same id will be
     * combined into frames to create an animation.
     *
     * The tiles are expected to be parsed per row, beginning at the upper-left
     * corner. Specifying the tiles in this manner is both concise and helps to
     * eliminate human error and maintenance concerns that may arise from the
     * manual specification of tile coordinates.
     *
     * @return  {Array.<Array.<string,number>>}  tile definition
     */
    'public getTileDefinition': [],


    /**
     * Retrieve tile dimensions
     *
     * This method should return an array containing three values: the width and
     * height of the individual tiles and the number of tiles per row. From this
     * data, any individual tile can be extracted from the tile set.
     *
     * @return  {Array.<number>}  tile width, tile height, tiles per row
     */
    'public getTileDimensions': []
} );
