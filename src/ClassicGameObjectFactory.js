/**
 * Creates game objects for the classic game
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class             = require( 'easejs' ).Class,
      GameObjectFactory = require( './GameObjectFactory' ),
      GameObject        = require( './gameobjs/GameObject' ),
      Tank              = require( './gameobjs/Tank' );


module.exports = Class( 'ClassicGameObjectFactory' )
    .implement( GameObjectFactory )
    .extend(
{
    'private _objs': {},


    __construct: function()
    {
        this._initObjs();
    },


    'public createObject': function( id )
    {
        return ( this._objs[ id ] || GameObject )( id );
    },


    'private _initObjs': function()
    {
        this._objs = {
            dirt:  null,
            base: null,
            water: null,
            block: null,
            mblock: null,
            brick: null,

            tup:   Tank,
            tright: Tank,
            tdown: Tank,
            tleft: Tank,

            atup: null,
            atright: null,
            atdown: null,
            atleft: null,

            mirrorul: null,
            mirrorur: null,
            mirrordr: null,
            mirrordl: null,

            owup: null,
            owright: null,
            owdown: null,
            owleft: null,

            cblock: null,

            rmirrorul: null,
            rmirrorur: null,
            rmirrordr: null,
            rmirrordl: null,

            ice: null,
            thinice: null,
        };
    },
} );
