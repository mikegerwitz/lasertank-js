/**
 * Handles the loading of LVL files
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * Levels are stored in a fixed-width LVL file.
 */

const Class = require( 'easejs' ).Class;


/**
 * Handles delegation of LVL data
 *
 * This acts as a Level factory, leaving all processing responsibilities to
 * the Level instance. Consequently, any custom level format would be
 * supported, provided that the appropriate Level is handling it.
 */
module.exports = Class( 'LevelSet',
{
    /**
     * Raw level set data (binary)
     * @type {string}
     */
    'private _data': '',

    /**
     * Constructor used to create new level instances
     * @type {Function}
     */
    'private _levelCtor': null,

    /**
     * Number of levels in the given LVL data
     * @type {number}
     */
    'private _levelCount': 0,


    /**
     * Initialize level set with LVL data and a Level constructor
     *
     * The Level constructor is used in place of a separate Level factory.
     *
     * @param  {string}  data      binary LVL data
     * @param  {Level}   level_ctor  Level constructor
     */
    __construct: function( data, level_ctor )
    {
        this._data    = ''+( data );
        this._levelCtor = level_ctor;

        // perform a simple integrity check on the provided data
        this._levelDataCheck();
    },


    /**
     * Perform a simple level data integrity check
     *
     * This is intended to throw an error if we believe the LVL data to be
     * invalid, or if the LVL data is invalid for the given Level
     * constructor.  The check will simply ensure that the level size (in
     * bytes) divides into the total LVL size (in bytes) without any
     * remainder.
     *
     * This is by no means fool-proof, but it should catch most.
     *
     * @return  {undefined}
     */
    'private _levelDataCheck': function()
    {
        const n = ( this._data.length / this._levelCtor.getLevelSize() );

        // if the result is not an integer, then it is either not an LVL,
        // the file is corrupt, or we were given the wrong Level constructor
        if ( n % 1 )
        {
            throw Error( 'Invalid or corrupt LVL data' );
        }

        // we already calculated it, so we may as well store it
        this._levelCount = n;
    },


    /**
     * Load a level by the given number (e.g. level 1)
     *
     * @param  {number}  id  number of level to load, 1-indexed
     */
    'public getLevelByNumber': function( id )
    {
        return this._levelCtor( this._data, id );
    },


    /**
     * Retrieve the number of levels in the LVL file
     *
     * @return  {number}  number of levels
     */
    'public getLevelCount': function()
    {
        return this._levelCount;
    },
} );
