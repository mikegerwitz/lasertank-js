/**
 * Handles level boundaries for collision detection and movement
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class = require( 'easejs' ).Class,
      Level   = require( './Level' );


/**
 * Calculates level bounding box
 *
 * This simply encapsulates the process of determining whether a given
 * position is against an edge of the level.
 */
module.exports = Class( 'LevelBounds',
{
    /**
     * Level width (number of tiles)
     * @type {number}
     */
    'private _mw': 0,

    /**
     * Level height (number of tiles)
     * @type {number}
     */
    'private _mh': 0,


    /**
     * Initialize bounding box for a given level
     *
     * @param  {Level}  level  level for which bounds should be calculated
     */
    __construct: function( level )
    {
        if ( !( Class.isA( Level, level ) ) )
        {
            throw TypeError( 'Invalid Level provided' );
        }

        // we are only interested in the dimensions of the level
        const dimen = level.getDimensions();

        this._mw = dimen[ 0 ];
        this._mh = dimen[ 1 ];
    },


    /**
     * Retrieve the tile position above the given position
     *
     * If the given tile position is at the top of the level, then the given
     * position will be returned.
     *
     * @param  {number}  pos  original tile position
     *
     * @return  {number}  tile position above given position or current position
     */
    'public getUpperPos': function( pos )
    {
        return ( this.isAtTop( pos ) )
            ? pos
            : pos - 1;
    },


    /**
     * Retrieve the tile position below the given position
     *
     * If the given tile position is at the bottom of the level, then the
     * given position will be returned.
     *
     * @param  {number}  pos  original tile position
     *
     * @return  {number}  tile position below given position or current position
     */
    'public getLowerPos': function( pos )
    {
        return ( this.isAtBottom( pos ) )
            ? pos
            : pos + 1;
    },


    /**
     * Retrieve the tile position to the left of the given position
     *
     * If the given tile position is at the leftmost column of the level,
     * then the given position will be returned.
     *
     * @param  {number}  pos  original tile position
     *
     * @return  {number}  tile position left of given position or current
     *                    position
     */
    'public getLeftPos': function( pos )
    {
        return ( this.isAtLeft( pos ) )
            ? pos
            : pos - this._mh;
    },


    /**
     * Retrieve the tile position to the right of the given position
     *
     * If the given tile position is at the rightmost column of the level,
     * then the given position will be returned.
     *
     * @param  {number}  pos  original tile position
     *
     * @return  {number}  tile position right of given position or current
     *                    position
     */
    'public getRightPos': function( pos )
    {
        return ( this.isAtRight( pos ) )
            ? pos
            : pos + this._mh;
    },


    /**
     * Determines if the given position is in the topmost row of the level
     *
     * @param  {number}  pos  tile position
     *
     * @return  {boolean}  true if within topmost row, otherwise false
     */
    'public isAtTop': function( pos )
    {
        // since tile positions are zero-indexed, we know that we're at the
        // top if the level height divides the position
        return ( pos % this._mh === 0 );
    },


    /**
     * Determines if the given position is in the bottom row of the level
     *
     * @param  {number}  pos  tile position
     *
     * @return  {boolean}  true if within bottom row, otherwise false
     */
    'public isAtBottom': function( pos )
    {
        // this "works" because tile positions are vertically indexed
        return this.isAtTop( pos + 1 );
    },


    /**
     * Determines if the given position is in the leftmost column of the
     * level
     *
     * @param  {number}  pos  tile position
     *
     * @return  {boolean}  true if within leftmost column, otherwise false
     */
    'public isAtLeft': function( pos )
    {
        // check if index is within the left-most column
        return ( pos <= this._mh );
    },


    /**
     * Determines if the given position is in the rightmost column of the
     * level
     *
     * @param  {number}  pos  tile position
     *
     * @return  {boolean}  true if within rightmost column, otherwise false
     */
    'public isAtRight': function( pos )
    {
        // check if index is within the right-most column
        return ( pos >= ( this._mh * ( this._mw - 1 ) ) );
    },
} );
