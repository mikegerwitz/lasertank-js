/**
 * Represents a map (level)
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * The details on exactly how the level data is stored is left to specific
 * implementations. However, the following is common to each file format:
 *
 *   - All game objects for the playfield should be returned in columns rather
 *     than rows. This is an artifact from the original game and is left as is
 *     because it is no more difficult to work with than rows (just less
 *     natural).
 *   - Tunnels are identified by the bitmask 0x40 and their id can be calculated
 *     by XORing with the bitmask and then dividing by two. For example, a
 *     tunnel identified by index 0 is 0x40, index 1 is 0x42, and so on.
 */

const Interface = require( 'easejs' ).Interface;


/**
 * Represents a game level
 *
 * Levels simply act as basic wrappers around a set of maps, returning only the
 * data associated with the requested level. This allows the data to be lazily
 * sliced out of the level file.
 *
 * Note that this interface specifies a constructor definition; this allows it
 * to be used in place of a separate Factory class.
 */
module.exports = Interface( 'Level',
{
    /**
     * Initialize level with level data and the given id
     *
     * @param  {LevelSet}  set  level set data
     * @param  {number}    id   1-indexed level id
     */
    __construct: [ 'set', 'id' ],


    /**
     * Retrieve game objects
     *
     * The game objects are expected to be returned in a manner consistent with
     * the original sources - in columns, not rows. The reason for this is that
     * the original game uses a multi-dimensional array [x][y], which creates
     * an array of columns (TPLAYFIELD, LTANK.H).
     *
     * @return  {Array.<number>}  array of game objects
     */
    'public getObjects': [],


    /**
     * Retrieve level dimensions
     *
     * @return  {Array.<number>}  width and height in tiles
     */
    'public getDimensions': [],


    /**
     * Retrieve level of object codes to their appropriate tiles
     *
     * @return  {Array.<string>}
     */
    'public getObjectTileLevel': [],


    /**
     * Retrieve tunnel color
     *
     * The color will be rendered in the background and will bleed through the
     * transparent portions of the tile.  The color may be specified in any CSS
     * format, but explicit values are recommended since color names (e.g.
     * 'green') can vary between environments.
     *
     * @param  {number}  object_id  tunnel object id
     *
     * @return  {string}  tunnel color or reasonable default if invalid obj id
     */
    'public getTunnelColor': [ 'object_id' ],


    /**
     * Determines if the given object is a tunnel
     *
     * @return  {boolean}  true if tunnel, otherwise false
     */
    'public isObjectTunnel': [ 'object_id' ],


    /**
     * Retrieve level name
     *
     * @return  {string}  level name
     */
    'public getLevelName': [],


    /**
     * Retrieve level author name
     *
     * @return  {string}  level author name
     */
    'public getLevelAuthor': [],


    /**
     * Retrieve level hint
     *
     * @return  {string}  level hint
     */
    'public getLevelHint': [],


    /**
     * Retrieve level difficulty
     *
     * The level difficulty should be returned as a 0-indexed value between
     * 0 and 4, with 0 representing "kids" and 4 representing "deadly".
     *
     * @return  {number}  0-indexed difficulty level
     */
    'public getLevelDifficulty': [],


    /**
     * Retrieve size of level in bytes
     *
     * @return  {number}  size of level in bytes
     */
    'public static getLevelSize': []
} );
