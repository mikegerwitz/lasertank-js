/**
 * Represents an action being performed on a game object
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Class       = require( 'easejs' ).Class,
      LevelBounds = require( './LevelBounds' );


module.exports = Class( 'LevelAction',
{
    // arranged by keycode
    'const D__MIN':  0,
    'const D_LEFT':  0,
    'const D_UP':    1,
    'const D_RIGHT': 2,
    'const D_DOWN':  3,
    'const D__MAX':  3,


    'private _dir': 0,
    'private _srcPos': 0,
    'public srcPos': 0,

    'private _bounds': null,

    'private _moveCallback':  null,


    __construct: function( bounds,  move_callback )
    {
        if ( !( Class.isA( LevelBounds, bounds ) ) )
        {
            throw TypeError( 'Invalid LevelBounds provided' );
        }

        this._dir = this.__self.$( 'D_UP' );

        this._bounds        = bounds;
        this._moveCallback  = move_callback;
    },


    'public move': function()
    {
        const method = [
            'getLeftPos',
            'getUpperPos',
            'getRightPos',
            'getLowerPos',
        ][ this._dir ];

        this._moveCallback(
            this._bounds[ method ].call( this._bounds, this.srcPos )
        );
    },


    set direction( todir )
    {
        todir = +todir;

        // ensure valid direction was provided (use >= and <= check because
        // todir could be NaN, etc)
        if ( !( ( todir >= this.__self.$( 'D__MIN' ) )
            && ( todir <= this.__self.$( 'D__MAX' ) )
        ) )
        {
            throw Error( 'Invalid tank direction' );
        }

        this._dir = todir;
    },


    get direction()
    {
        return this._dir;
    },
} );
