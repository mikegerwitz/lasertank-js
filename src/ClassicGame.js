/**
 * Interface representing a facade for a game type
 *
 *  Copyright (C) 2012, 2015 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


const Class                    = require( 'easejs' ).Class,
      Game                     = require( './Game' ),
      ClassicGameObjectFactory = require( './ClassicGameObjectFactory' ),
      ClassicTileDfn           = require( './ClassicTileDfn' ),
      LtgLoader                = require( './LtgLoader' ),
      ClassicLevel             = require( './level/ClassicLevel' ),
      LevelBounds              = require( './level/LevelBounds' ),
      LevelRender              = require( './level/LevelRender' ),
      LevelSet                 = require( './level/LevelSet' ),
      LevelState               = require( './level/LevelState' ),
      TileMasker               = require( './TileMasker' );


/**
 * Facade for the classic (original) game of LaserTank
 */
module.exports = Class( 'ClassicGame' )
    .implement( Game )
    .extend(
{
    /**
     * LTG loader
     * @type {LtgLoader}
     */
    'private _ltgLoader': null,

    /**
     * Tile masker
     * @type {TileMasker}
     */
    'private _masker': null,

    /**
     * Tiles
     * @type {Object.<Object>}
     */
    'private _tileSet': null,

    /**
     * Set of available levels
     * @type {LevelSet}
     */
    'private _levelSet': null,

    /**
     * Event handlers
     * @type {Object.<Function>}
     */
    'private _callback': {},

    /**
     * Performs level rendering
     * @type {LevelRender}
     */
    'private _render': null,

    /**
     * Classic game object factory
     * @type {ClassicGameObjectFactory}
     */
    'private _gameObjFactory': null,


    /**
     * Initialize game with LTG and LVL data
     *
     * The LTG and LVL data can be changed at any time, but are required in
     * the constructor because they are needed in order for the game to be
     * functional.
     *
     * DOCUMENT is used internally for creating elements; the DOM will not
     * be manipulated.
     *
     * @param  {HTMLDocument}  document  DOM document
     *
     * @param  {string}  ltg_data  binary string containing LTG file data
     * @param  {string}  lvl_data  binary string containing LVL file data
     */
    __construct: function( document, ltg_data, lvl_data )
    {
        const _self = this;

        this._ltgLoader = LtgLoader();
        this._masker    = TileMasker( ClassicTileDfn(), document );

        this._gameObjFactory = ClassicGameObjectFactory();

        // load initial tile and level data from the LTG and LVL data
        this.setTileData( ltg_data, function()
        {
            this.setLevelData( lvl_data, function()
            {
                _self._trigger( 'ready' );
            } );
        } );
    },


    /**
     * Render to the given 2d canvas context
     *
     * EVENT_TARGET will be monitored for keypresses.
     *
     * @param  {CanvasRenderingContext2d}  ctx           2d canvas context
     * @param  {*}                         event_target  keyboard event target
     *
     * @return  {ClassicGame}  self
     */
    'public renderTo': function( ctx, event_target )
    {
        // if there is a previous renderer, free its canvas before
        // continuing (to both clean up and to free any locks, allowing for
        // tile set and level changes)
        if ( this._render )
        {
            this._render.clearCanvas();
        }

        const level       = this._levelSet.getLevelByNumber( 1 ),
              level_state = LevelState( level, this._gameObjFactory ),
              bounds      = LevelBounds( level );

        // render the first level (hardcoded for now)
        this._render = LevelRender( ctx, this._tileSet )
            .render( level, level_state );

        // POC
        event_target.onkeydown = function( event )
        {
            let dir;

            switch ( event.keyCode )
            {
                case 37:
                case 38:
                case 39:
                case 40:
                    dir = event.keyCode - 37;
                    break;

                default:
                    return;
            }

            event.preventDefault();
            level_state.movePlayer( dir, bounds );
        };

        return this;
    },


    /**
     * Set LTG data for tiles
     *
     * @param  {string}      data      binary string containing LTG data
     * @param  {function()}  callback  function to call when complete
     *
     * @return  {ClassicGame}  self
     */
    'public setTileData': function( data, callback )
    {
        // get tile metadata
        const _self = this,
              meta  = this._ltgLoader.fromString( data );

        this._masker.getMaskedTiles( meta.tiles, meta.mask, function( tdata )
        {
            _self._tileSet = tdata;
            callback.call( _self.__inst );
        } );

        return this;
    },


    /**
     * Set LVL data for levels
     *
     * @param  {string}      data      binary string containing LVL data
     * @param  {function()}  callback  function to call when complete
     *
     * @return  {ClassicGame}  self
     */
    'public setLevelData': function( data, callback )
    {
        this._levelSet = LevelSet( data, ClassicLevel );

        callback.call( this.__inst );
        return this;
    },


    /**
     * Attach event handler
     *
     * This is a very basic system and allows for only a single event to be
     * attached at a time (that is all that is needed at this point)
     *
     * @param  {string}    name      event name
     * @param  {Function}  callback  event callback
     *
     * @return  {ClassicGame}  self
     */
    'public on': function( name, callback )
    {
        this._callback[ name ] = callback;
        return this;
    },


    /**
     * Trigger an event, invoking its callback
     *
     * @param  {string}  name  event name
     *
     * @return  {ClassicGame}  self
     */
    'private _trigger': function( name )
    {
        if ( typeof this._callback[ name ] === 'function' )
        {
            this._callback[ name ].call( this.__inst );
        }
    }
} );
